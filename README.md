# Oitivas

Projeto relacionado à:
- Transcrição automática de uma oitiva realizada entre um delegado e depoente;
- Leitura de características físicas do depoente;
- Leitura de emoções do depoente.